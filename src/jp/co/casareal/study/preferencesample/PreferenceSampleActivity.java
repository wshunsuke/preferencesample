package jp.co.casareal.study.preferencesample;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

/**
 * リファレンスを利用するサンプル
 */
public class PreferenceSampleActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		findViewById(R.id.button).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(PreferenceSampleActivity.this, NextActivity.class));
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		String fileName = "sample";

		// ---------------------------------書き込み　▼ココカラ▼
		// プリファレンスの取得
		// MODE_PRIVATEなら「-rw-rw----」
		// MODE_WORLD_READABLEなら「-rw-rw-r--」
		// MODE_WORLD_WRITEABLEなら「-rw-rw--w-」
		SharedPreferences prefs = getSharedPreferences(fileName,
				MODE_WORLD_READABLE);

		// Editorオブジェクトの取得
		Editor editor = prefs.edit();

		Date date = new Date();

		// 値の格納
		editor.putLong("date_long", date.getTime());
		editor.putString("date_string", new SimpleDateFormat(
				"yyyy月MM日dd日(E)hh:mm:ss").format(date));

		// 変更の確定
		editor.commit();
		editor.clear();
		Toast t1 = Toast.makeText(this, "書き込み完了", Toast.LENGTH_SHORT);
		t1.setGravity(Gravity.CENTER, 0, 0);
		t1.show();
		// ---------------------------------書き込み　▲ココマデ▲

		// ---------------------------------読み込み　▼ココカラ▼
		String date_string = prefs.getString("date_string", "NA");
		Log.v("Preference読み込み", "date_string = " + date_string);
		long date_long = prefs.getLong("date_long", 0L);
		Log.v("Preference読み込み", "date_long = " + date_long);
		Toast t2 = Toast.makeText(this, "読み込み完了（LogCatを見てください）", Toast.LENGTH_SHORT);
		t2.setGravity(Gravity.BOTTOM, 0, 0);
		t2.show();
		// ---------------------------------読み込み　▲ココマデ▲

		// ======== アクティビティプリファレンスの場合 ========
		SharedPreferences activityPrefs = getPreferences(MODE_PRIVATE);
		activityPrefs.edit().putInt("int_val", 12345).commit();
		Log.v("Preference読み込み",
				"int_val = " + activityPrefs.getInt("int_val", 0));

		// ======== デフォルトプリファレンスの場合 ========
		SharedPreferences defaultPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		defaultPrefs.edit().putFloat("float_val", 987.654F).commit();
	}
}
/*

以下のようなsample.xmlが
/data/data/jp.co.casareal.study.preferencesample/shared_prefs ディレクトリに格納される。

<?xml version='1.0' encoding='utf-8' standalone='yes' ?>
<map>
<string name="date_string">2011月12日16日(金)03:04:43</string>
<long name="date_long" value="1324015483486" />
</map>
*/