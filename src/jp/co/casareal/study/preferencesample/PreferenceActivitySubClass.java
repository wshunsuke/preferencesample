package jp.co.casareal.study.preferencesample;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * PreferenceActivityのサンプル
 *
 * 1./res/xmlディレクトリに画面定義ファイルを作る。
 * 2.addPreferencesFromResourceメソッドで上記1.を指定する。
 * 3./data/data/<パッケージ>/shared_prefsディレクトリに、「<パッケージ>+_preferences.xml」ファイルができる。
 * 4.モードは「MODE_PRIVATE | MODE_APPEND」固定。
 *
 */
public class PreferenceActivitySubClass extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// PreferenceActivityであれば、このメソッドで画面からプリファレンス作成できる！
		addPreferencesFromResource(R.xml.pref);
	}
}
