package jp.co.casareal.study.preferencesample;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class NextActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.next);

		// PreferenceSampleActivityのアクティビティプリファレンスをアクセス
		SharedPreferences prefs0 = getSharedPreferences("PreferenceSampleActivity", MODE_PRIVATE);
		Log.v("NextActivity読み込み", "int_val = " + prefs0.getInt("int_val", 0));

		// PreferenceSampleActivityのデフォルトプリファレンスをアクセス
		SharedPreferences prefs1 = getSharedPreferences("jp.co.casareal.study.preferencesample_preferences", MODE_PRIVATE);
		Log.v("NextActivity読み込み", "float_val = " + prefs1.getFloat("float_val", 0F));

		findViewById(R.id.button).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(NextActivity.this, PreferenceActivitySubClass.class));
			}
		});
	}
}
